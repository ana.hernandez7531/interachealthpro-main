DROP TABLE "transaction";
DROP TABLE "invoice";
DROP TABLE "paymentInfo";
DROP TABLE "employee";
DROP TABLE "employeeGroup";
DROP TABLE "businessAcc";
DROP TABLE "client";

DROP TYPE "authType";
DROP TYPE "typeCard";

CREATE TYPE "authType" AS ENUM ('Facebook', 'Gmail', 'Apple', 'Email');
CREATE TYPE "typeCard" AS ENUM ('Visa', 'MasterCard', 'American Express');



CREATE TABLE "client"(
    "idAcc" VARCHAR(50) PRIMARY KEY, 
    "lastName" VARCHAR(50) NOT NULL, 
    "name" VARCHAR(50) NOT NULL, 
    "email" VARCHAR(50) NOT NULL, 
    "lastUpdateDate" DATE NULL,
    "authType" "authType" NOT NULL, --('Facebook', 'Gmail', 'Apple','Email')--
    "accActive" BOOLEAN NOT NULL --( 1 -true -> Active, 0 -false-> Deactivated)--
);

CREATE TABLE "businessAcc" (
    "idAccEnt" VARCHAR(50) PRIMARY KEY, 
    "nameEnt" VARCHAR(50) NOT NULL,
    "email" VARCHAR(50) NOT NULL UNIQUE,
    "nameContact" VARCHAR(50) NOT NULL, 
    "emailContact" VARCHAR(50) NOT NULL, 
    "telNumber" CHAR(11) NOT NULL, 
    "lastUpdateDate" DATE NULL,
    "accActive" BOOLEAN NOT NULL --( 1 -true -> Active, 0 -false-> Deactivated)--
);

CREATE TABLE "employeeGroup"(
    "idGroup" VARCHAR(50) PRIMARY KEY,
    "groupName" VARCHAR(50) NOT NULL,
    "accActive" BOOLEAN NOT NULL,
    "lastUpdateDate" DATE NOT NULL,
    "idAccEnt"  VARCHAR(50) NOT NULL REFERENCES "businessAcc"("idAccEnt")
);

CREATE TABLE "employee"(
    "idAcc" VARCHAR(50) REFERENCES "client"("idAcc"),
    "idAccEnt"  VARCHAR(50) REFERENCES "businessAcc"("idAccEnt"),
    "idGroup"  VARCHAR(50) REFERENCES "employeeGroup"("idGroup"),
    "accActive" BOOLEAN NOT NULL,
    "lastUpdateDate" DATE NOT NULL,
    PRIMARY KEY ("idAcc", "idAccEnt")
);

CREATE TABLE "paymentInfo" (
    "idPaymentInfo" VARCHAR(50) PRIMARY KEY, 
    "cardHolder" VARCHAR(50) NULL,
    "numCard" BIGINT NULL,
    "typeCard" "typeCard" NULL, --(Visa, MasterCard, American Express)--
    "expDate" BIGINT NULL, --(MMYY)--
    "secCode" BIGINT NULL,
    "accActive" BOOLEAN NOT NULL,
    "lastUpdateDate" DATE NOT NULL,
    "idAcc"  VARCHAR(50) REFERENCES "client"("idAcc") NULL,
    "idAccEnt" VARCHAR(50) REFERENCES "businessAcc"("idAccEnt") NULL
);

create table "invoice"(
    "idInvoice" VARCHAR(50) PRIMARY KEY,
    "description" VARCHAR NOT NULL,
    "price" decimal NOT NULL,
    "isPayed" boolean not null,
    "lastUpdate" date not null,
    "idAcc" VARCHAR(50) not null REFERENCES "client" ("idAcc")
);

create table "transaction"(
    "idTransaction" VARCHAR(50) primary key,
    "dateTransaction" date not null,
    "isApproved" boolean not null,
    "idAcc" VARCHAR(50) REFERENCES "client" ("idAcc"),
    "idAccEnt" VARCHAR(50) REFERENCES "businessAcc" ("idAccEnt"),
    "idInvoice" VARCHAR(50) not null REFERENCES "invoice" ("idInvoice")
);