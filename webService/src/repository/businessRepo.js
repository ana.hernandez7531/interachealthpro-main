import { Business } from "../models/Business.js";

export function findOneById(idBusiness, isActive){
    return Business.findOne({
        where : {
            idAccEnt : idBusiness,
            accActive : isActive === undefined ? [true, false] : [isActive]
        }
    })
}
