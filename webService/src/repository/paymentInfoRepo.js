import { PaymentInfo } from "../models/PaymentInfo.js";

export function findAll(idAcc, idAccEnt, isActive = undefined){
  return PaymentInfo.findAll({
    where : {
      idAcc,
      idAccEnt,
      accActive : isActive === undefined ? [true, false] : [isActive]
    }
  })
}

export function findOneById(idPaymentInfo, idAcc, idAccEnt, isActive = undefined){
  return PaymentInfo.findOne({
    where : {
      idPaymentInfo,
      idAcc,
      idAccEnt,
      accActive : isActive === undefined ? [true, false] : [isActive]
    }
  })
}

export function findOneByCardNumber(cardNumber, idAcc, idAccEnt, isActive = undefined){
  return PaymentInfo.findOne({
    where : {
      idAcc,
      idAccEnt,
      numCard : cardNumber,
      accActive : isActive === undefined ? [true, false] : [isActive]
    }
  })
}


