import { BOOLEAN, DATE, INTEGER, STRING } from "sequelize";
import { sequelize } from "../config/sequelizeConfig.js";

export const Employee = sequelize.define(
  "Employee",
  {
    idAcc: {
      type: STRING,
      primaryKey: true,
      allowNull: false,
      validate: {
        notEmpty : true
      },
    },

    idAccEnt: {
      type: STRING,
      allowNull: false,
      validate: {
        notEmpty : true
      },
    },

    idGroup : {
        type : STRING,
        allowNull : true,
        validate : {
            notEmpty : true
        }
    },

    accActive : {
        type : BOOLEAN,
        allowNull : false,
        validate : {
            isIn : [[true, false]]
        }
    },

    lastUpdateDate : {
        type : DATE,
        allowNull : false,
        validate : {
            isDate : true
        }
    }
  },

  {
    timestamps: false,
    tableName: "employee",
  }
);


