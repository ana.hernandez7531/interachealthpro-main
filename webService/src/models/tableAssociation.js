import { Account } from "./Account.js";
import { Business } from "./Business.js";
import { Employee } from "./Employee.js";
import { EmployeeGroup } from "./EmployeeGroup.js";
import { Invoice } from "./Invoice.js";
import { Transaction } from "./Transaction.js";

Account.hasMany(Employee, { foreignKey: "idAcc" });
Employee.belongsTo(Account, { foreignKey: "idAcc" });

Employee.belongsTo(Business, { foreignKey: "idAccEnt" });
Business.hasMany(Employee, { foreignKey: "idAccEnt" });

Employee.belongsTo(EmployeeGroup, { foreignKey: "idGroup" });
EmployeeGroup.hasMany(Employee, { foreignKey: "idGroup" });

Invoice.belongsTo(Account, {foreignKey : "idAcc"})
Account.hasMany(Invoice, {foreignKey : "idAcc"})

Transaction.belongsTo(Invoice, {foreignKey : "idInvoice"})
Invoice.hasMany(Transaction, {foreignKey : "idInvoice"})

Transaction.belongsTo(Business, {foreignKey : "idAccEnt"})
Business.hasMany(Transaction, {foreignKey : "idAccEnt"})

Transaction.belongsTo(Account, {foreignKey : "idAcc"})
Account.hasMany(Transaction, {foreignKey : "idAcc"})
