import { BOOLEAN, DataTypes, DATE, INTEGER, STRING } from "sequelize";
import { sequelize } from "../config/sequelizeConfig.js";

export const Business = sequelize.define(
  "Business",
  {
    idAccEnt: {
      type: STRING,
      primaryKey: true,
      allowNull: false,
      validate: {
        notEmpty : true
      }
    },

    nameEnt: {
      type: STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      }
    },

    email: {
      type: STRING,
      allowNull: false,
      validate: {
        isEmail: true,
      },
    },

    nameContact: {
      type: STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      }
    },


    emailContact: {
      type: STRING,
      allowNull: false,
      validate: {
        isEmail: true,
      },
    },

    telNumber: {
      type: INTEGER,
      allowNull: false,
      validate: {
        len: [10, 11],
      },
    },

    lastUpdateDate: {
      type: DATE,
      allowNull: false,
      validate: {
        isDate: true,
      },
    },

    accActive: {
      type: BOOLEAN,
      allowNull: false,
      validate: {
        isIn: [[true, false]],
      },
    },
  },

  {
    timestamps: false,
    tableName: "businessAcc",
  }
);
