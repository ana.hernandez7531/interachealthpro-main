import { BOOLEAN, DATE, DECIMAL, INTEGER, STRING } from "sequelize"
import { sequelize } from "../config/sequelizeConfig.js"

export const Invoice = sequelize.define(
    "Invoice",
    {
        idInvoice : {
            type : STRING,
            primaryKey : true,
            allowNull : false, 
            validate : {
                notEmpty : true
            }
        },

        description : {
            type : STRING,
            allowNull : false
        },

        price : {
            type : DECIMAL,
            allowNull : false, 
            validate : {
                isDecimal : true
            }
        },

        isPayed : {
            type : BOOLEAN,
            allowNull : false,
            validate : {
                isIn : [[true, false]]
            }
        },

        lastUpdate : {
            type : DATE,
            allowNull : false,
            validate : {
                isDate : true
            }
        },

        idAcc : {
            type : STRING,
            allowNull : false, 
            validate : {
                notEmpty : true
            }
        }
    }, 
    {
        tableName : "invoice",
        timestamps : false
    }
) 