import { DataTypes, INTEGER, STRING } from "sequelize";
import { sequelize } from "../config/sequelizeConfig.js";
import { TypeCard } from "../enums/TypeCard.js";

export const PaymentInfo = sequelize.define(
  "PaymentInfo",
  {
    idPaymentInfo: {
      type: STRING,
      primaryKey: true,
      allowNull: false,
      validate: {
        notEmpty : true
      },
    },

    cardHolder: {
      type: STRING,
      allowNull: true,
      validate: {
        // isAlpha: true,
        len: [2, 50],
      },
    },

    numCard: {
      type: INTEGER,
      allowNull: true,
      defaultValue: null,
      validate: {
        //len: [15, 16],
        // isCreditCard: true,
      },
    },

    typeCard: {
      type: STRING,
      allowNull: true,
      validate: {
        isIn: [[TypeCard.VISA, TypeCard.MASTERCARD, TypeCard.AMERICANEXPRESS]],
      },
    },

    expDate: {
      type: INTEGER,
      allowNull: true,
      defaultValue: null,
      validate: {
        len: [3, 4],
        isInt: true,
      },
    },

    secCode: {
      type: INTEGER,
      allowNull: true,
      defaultValue: null,
      validate: {
        len: [3, 4],
        isInt: true,
      },
    },

    accActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      validate: {
        isIn: [[true, false]],
      },
    },

    lastUpdateDate: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notEmpty: true,
        isDate: true,
      },
    },

    idAcc: {
      type: STRING,
      allowNull: true,
      validate: {
        notEmpty : true
      },
    },

    idAccEnt: {
      type: STRING,
      allowNull: true,
      validate: {
        notEmpty : true
      },
    },
  },

  {
    tableName: "paymentInfo",
    timestamps: false,
  }
);
