
export class NotAuthorizedError extends Error{
    constructor(message = "You are not authorized to use this resource."){
        super(message)
        this.name = this.constructor.name
    }
}