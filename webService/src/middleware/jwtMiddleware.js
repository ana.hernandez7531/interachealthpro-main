import jwt from "jsonwebtoken"
import { refreshToken } from "../services/keycloakService.js"

export async function jwtMiddleware (req, res, next){
    try{
        const {access_token, refresh_token} = req.cookies

        if(access_token)
            decodeToken(req, access_token)

        if(!access_token && refresh_token){
            const response = await refreshToken(refresh_token)

            if(response){
                res.cookie("access_token", response.access_token, {maxAge : 1000 * response.expires_in})
                res.cookie("refresh_token", response.refresh_token, {maxAge : 1000 * response.refresh_expires_in})
                decodeToken(req, response.access_token)
            }
        }
    } catch(error){
        console.log(error)
    } 
    next()
}

function decodeToken(req, accessToken){
    const decodedToken = jwt.decode(accessToken)
    req.session = {
        role : decodedToken?.role,
        email : decodedToken?.email,
        idUser : decodedToken?.sub
    }
}