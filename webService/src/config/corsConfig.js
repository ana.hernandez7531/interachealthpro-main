import cors from "cors"
import * as dotenv from "dotenv"

dotenv.config()
const {CORS_ORIGIN} = process.env

export const corsConfig = cors({
    origin : CORS_ORIGIN,
    credentials : true
})