import { Router } from "express";
import * as groupEmployeeService from "../services/employeeGroupService.js";
import * as AuthMiddleware from "../middleware/authMiddleware.js"
import { AccountType } from "../enums/AccountType.js";

export const employeeGroupRoute = Router();
employeeGroupRoute.use(AuthMiddleware.isAuthenticated)
employeeGroupRoute.use(AuthMiddleware.isRole([AccountType.BUSINESS]))
employeeGroupRoute.get("/getAll", getAll);
employeeGroupRoute.post("/createGroup", createEmployeeGroup);
employeeGroupRoute.put("/updateGroup", updateGroup);

async function getAll(req, res, next) {
  try {
    const {idUser} = req.session
    const employeeGroupList = await groupEmployeeService.getAllEmployeeGroups(idUser);
    res.json(employeeGroupList);
  } catch (error) {
    next(error);
  }
}

async function createEmployeeGroup(req, res, next) {
  try {
    const {idUser} = req.session
    const {groupName} = req.body

    await groupEmployeeService.createEmployeeGroup(idUser, groupName);
    res.json({message : "Employee group created"});

  } catch (error) {
    next(error);
  }
}

async function updateGroup(req, res, next) {
  try {
    const {idUser} = req.session
    const employeeGroupUpdated = await groupEmployeeService.updateEmployeeGroup(
      req.body.idGroup,
      req.body.groupName,
      idUser,
      req.body.accActive
    );
    res.json({message : "employee group updated"});
  } catch (error) {
    next(error);
  }
}
