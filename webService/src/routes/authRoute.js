import { Router } from "express";
import jwt from "jsonwebtoken"
import * as authService from "../services/authService.js";
import * as AuthMiddleware from "../middleware/authMiddleware.js"
import * as KeycloakService from "../services/keycloakService.js"

export const authRoute = Router();

authRoute.post("/register/client", [AuthMiddleware.isNotAuthenticated, registerClient]);
authRoute.post("/register/business",[AuthMiddleware.isNotAuthenticated, registerBusiness]);
authRoute.post("/login", [AuthMiddleware.isNotAuthenticated, login])
authRoute.get("/logout", logout)

async function registerClient(req, res, next) {
  try {
    await authService.registerClient(req.body);
    res.json({ message: "account created" });
  } catch (error) {
    next(error);
  }
}

async function registerBusiness(req, res, next) {
  try {
    await authService.registerBusiness(req.body);
    res.json({ message: "business account created" });
  } catch (error) {
    next(error);
  }
}

async function login(req, res, next){
  try{
    const {username, password} = req.body
    const response = await KeycloakService.login(username, password)
    const decodedToken = jwt.decode(response.access_token)

    res.cookie("access_token", response.access_token, {maxAge : 1000 * response.expires_in})
    res.cookie("refresh_token", response.refresh_token, {maxAge : 1000 * response.refresh_expires_in})
    res.json({
      idUser : decodedToken?.sub,
      email : decodedToken?.email,
      role : decodedToken?.role
    })

  } catch(error){
    next(error)
  }
}

async function logout(req, res, next){
  try{
    const {refresh_token} = req.cookies
    await KeycloakService.logout(refresh_token)

  } catch(error){
    console.log(error)
  }

  res.clearCookie("access_token")
  res.clearCookie("refresh_token")
  res.json({message : "logout succesfull"})
}
