import { Router } from "express";
import * as TransactionService from "../services/transactionService.js"
import * as AuthMiddleware from "../middleware/authMiddleware.js"

export const transactionRoute = Router()
transactionRoute.use(AuthMiddleware.isAuthenticated)
transactionRoute.post("/createOrder", createOrder)
transactionRoute.post("/capturePayment", capturePayment)

async function createOrder(req, res, next){
    try{
        const {idInvoice} = req.body
        const orderId = await TransactionService.createOrder(idInvoice)
        res.json({orderId}) 

    } catch(error){
        next(error)
    }
}

async function capturePayment(req, res, next){
    try{
        const {idUser, role} = req.session
        const {orderId} = req.body
        const transaction = await TransactionService.capturePayment(orderId, role, idUser)
        res.json(transaction)

    } catch(error){
        next(error)
    }
}
