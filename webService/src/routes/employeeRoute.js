import { Router } from "express"
import * as employeeService from "../services/employeeService.js"
import * as AuthMiddleware from "../middleware/authMiddleware.js"
import { AccountType } from "../enums/AccountType.js"

export const employeeRoute = Router()
employeeRoute.use(AuthMiddleware.isAuthenticated)
employeeRoute.use(AuthMiddleware.isRole([AccountType.BUSINESS]))

employeeRoute.get("/", getEmployees)
employeeRoute.post("/", createEmployee)
employeeRoute.put("/", updateEmployee)
employeeRoute.delete("/", deactivateEmployee)

async function getEmployees(req, res, next){
    try{
        const {idUser} = req.session

        const result = await employeeService.getEmployees(idUser)
        res.json(result)
    } catch(error){
        next(error)
    }
}

async function createEmployee(req, res, next){
    try{
        const {idAcc, idGroup, newAccount} = req.body
        const {idUser} = req.session
        
        if(newAccount)
            await employeeService.registerClientAndAddEmployee(idUser, idGroup, newAccount)
        else 
            await employeeService.createEmployee(idUser, idAcc, idGroup)

        res.json({message : "employee added"})

    } catch(error){
        next(error)
    }
}

async function updateEmployee(req, res, next){
    try{
        const {idAcc, idGroup} = req.body
        const {idUser} = req.session

        await employeeService.updateEmployee(idUser, idAcc, idGroup)
        res.json({message : "employee updated"})

    } catch(error){
        next(error)
    }
}

async function deactivateEmployee(req, res, next){
    try{
        const {idAcc} = req.body
        const {idUser} = req.session

        await employeeService.deactivateEmployee(idUser, idAcc)
        res.json({message : "Employee deleted"})

    } catch(error){
        next(error)
    }
}
