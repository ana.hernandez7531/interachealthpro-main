import { Router } from "express";
import * as InvoiceService from "../services/invoiceService.js"

export const invoiceRoute = Router()

invoiceRoute.get("/", getInvoices)
invoiceRoute.post("/", createInvoice)

async function getInvoices(req, res, next){
    try{
        const invoices = await InvoiceService.getAllInvoices()
        res.json(invoices)

    } catch(error){
        next(error)
    }
}

async function createInvoice(req, res, next){
    try{
        await InvoiceService.create(req.body)
        res.json({message : "Invoice created"})
    } catch(error){
        next(error)
    }
}