
export const AccountType = Object.freeze({
    CLIENT : "Client",
    BUSINESS : "Business"
})