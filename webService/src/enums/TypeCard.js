export const TypeCard = Object.freeze({
  VISA: "Visa",
  MASTERCARD: "MasterCard",
  AMERICANEXPRESS: "American Express",
});
