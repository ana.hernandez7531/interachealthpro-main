export const AuthType = Object.freeze({
    GMAIL : "Gmail",
    FACEBOOK : "Facebook",
    APPLE : "Apple",
    EMAIL : "Email"
})