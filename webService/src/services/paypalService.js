import paypal from "@paypal/checkout-server-sdk"
import { paypalClient } from "../config/paypalConfig.js"

export function createOrder(invoice){
    const request = new paypal.orders.OrdersCreateRequest()
    request.requestBody({
        intent : "CAPTURE",
        purchase_units : [
            {
                reference_id : invoice.idInvoice,
                description : invoice.description,
                amount : {
                    currency_code : "CAD",
                    value : invoice.price,
                }
            }
        ]
    })

    return paypalClient.execute(request)
}

export function capturePayment(orderId){
    const request = new paypal.orders.OrdersCaptureRequest(orderId)
    return paypalClient.execute(request)
}