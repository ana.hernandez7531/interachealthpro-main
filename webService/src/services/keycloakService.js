import * as dotenv from "dotenv"
import fetch from "node-fetch"
import { BadRequestError } from "../errors/BadRequestError.js"

dotenv.config()

const {
    KC_CLIENT_ID, 
    KC_CLIENT_SECRET, 
    KC_TOKEN_URL,
    KC_LOGOUT_URL,
    KC_ADMIN_URL
} = process.env

function option(body){
    return {
        method : "post",
        headers : {
            "content-type" : "application/x-www-form-urlencoded"
        },
        body : new URLSearchParams(body)
    }
}

export async function login(username, password){
    const response = await fetch(KC_TOKEN_URL, option({
        scope : "openid",
        grant_type : "password",
        client_id : KC_CLIENT_ID,
        client_secret : KC_CLIENT_SECRET,
        username,
        password
    }))

    if(!response.ok)
        throw new BadRequestError("Invalid credentials")

    return response.json()
}

export async function refreshToken(token){
    const response = await fetch(KC_TOKEN_URL, option({
        refresh_token : token,
        grant_type : "refresh_token",
        client_id : KC_CLIENT_ID,
        client_secret : KC_CLIENT_SECRET
    }))

    if(response.ok)
        return response.json()
    else
        return null
}

export async function logout(refresh_token){
    const response = await fetch(KC_LOGOUT_URL, option({
        client_id : KC_CLIENT_ID,
        client_secret : KC_CLIENT_SECRET,
        refresh_token : refresh_token
    }))

    return response.ok
}

export async function loginAsAdmin(){
    const response = await fetch(KC_TOKEN_URL, option({
        scope : "openid roles",
        grant_type : "client_credentials",
        client_id : KC_CLIENT_ID,
        client_secret : KC_CLIENT_SECRET
    }))

    if(!response.ok)
        throw new BadRequestError("Error on Keycloak server")

    const data = await response.json()
    return data.access_token
}

export async function registerUser(email, password, role, firstName = null, lastName = null){
    const access_token = await loginAsAdmin()

    const response = await fetch(KC_ADMIN_URL + "/users", {
        headers : {
            "content-type" : "application/json",
            "Authorization" : `Bearer ${access_token}`
        },
        body : JSON.stringify({
            email : email,
            username : email,
            firstName : firstName,
            lastName : lastName,
            attributes : {
                role : role
            },
            credentials : [
                {
                    type : "password",
                    value : password
                }
            ],
            enabled : true
        }),
        method : "post"
    }) 

    if(response.ok){
        const location = response.headers.get("location").split("/")
        return location[location.length - 1]
    } else
        return null
}