export const business = [
    {
        idAccEnt : "f02b19e0-a1e6-48ed-97c6-60fabebda006",
        nameEnt : "business 1",
        email : "business1@gmail.com",
        nameContact : "kavya i",
        emailContact : "kavya@gmail.com",
        telNumber : "5149974434",
        lastUpdateDate : "2023-01-23",
        accActive : true
    },  
    {
        idAccEnt : "bus-002",
        nameEnt : "business 2",
        email : "business2@gmail.com",
        nameContact : "risha k",
        emailContact : "risha@gmail.com",
        telNumber : "5142248734",
        lastUpdateDate : "2022-12-23",
        accActive : true
    } 
]