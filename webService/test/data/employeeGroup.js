export const employeeGroup = [
  {
    idGroup: "empG-001",
    groupName: "Marketing",
    accActive: true,
    lastUpdateDate: "2023-01-23",
    idAccEnt: "f02b19e0-a1e6-48ed-97c6-60fabebda006",
  },
  {
    idGroup: "empG-002",
    groupName: "Developer",
    accActive: true,
    lastUpdateDate: "2023-01-23",
    idAccEnt: "f02b19e0-a1e6-48ed-97c6-60fabebda006",
  },
  {
    idGroup: "empG-003",
    groupName: "Management",
    accActive: true,
    lastUpdateDate: "2023-01-23",
    idAccEnt: "bus-002",
  }
];
