import { expect } from "chai";
import { businessJwt } from "../index.test.js";
import { TypeCard } from "../../src/enums/TypeCard.js";
import chai from "chai";
import { app } from "../../src/app.js";

const route = "/paymentInfo"
const cookie = "access_token=" + businessJwt

describe(route, () => {

  context("GET", () => {

    it("200 - Get all payment info the logged in user:", async () => {
      const res = await chai.request(app)
        .get(route + "/getAllPaymentInfo")
        .set("Cookie", cookie)

      expect(res.status).is.equal(200);
    });
  });

  context("POST", () => {

    it("Payment info already exists in database", async () => {
      const body = {
        cardHolder: "Business 2",
        numCard: 1234567891234567,
        typeCard: "Visa",
        expDate: 1212,
        secCode: 212,
        accActive: true,
      };
      
      const res = await chai.request(app)
        .post(route + "/createPaymentInfo")
        .set("Cookie", cookie)
        .send(body)

      expect(() =>
        res.to.throw(
          "Payment information cannot be created since it already exists."
        )
      );
    });

    it("Should: 200 - Create new payment information.", async () => {
      const body = {
        cardHolder: "Business 2",
        numCard: 1234567891234588,
        typeCard: "MasterCard",
        expDate: 1212,
        secCode: 212,
        accActive: true,
      };
      
      const res = await chai.request(app)
        .post(route + "/createPaymentInfo")
        .set("Cookie", cookie)
        .send(body)

      expect(res.status).is.equal(200);
    });
  });

  context("PUT", () => {

    it("400 - The payment information was not found", async () => {
      const body = {
        idPaymentInfo: "100",
        cardHolder: "Ana Hernandez",
        numCard: 2016457898651575,
        typeCard: TypeCard.AMERICANEXPRESS,
        expDate: 1230,
        secCode: 567,
        accActive: true,
      };
      const res = await chai.request(app)
        .put(route + "/updatePaymentInfo")
        .set("Cookie", cookie)
        .send(body);

      expect(() => res.to.throw("The payment information was not found"));
    });

  });

  describe("DELETE", () => {

    it("400 - Payment information id does not exist", async () => {
      const body = {
        idPaymentInfo: "100",
      };

      const res = await chai.request(app)
        .delete(route + "/deletePaymentInfo")
        .set("Cookie", cookie)
        .send(body);

      expect(() => res.to.throw("The payment information was not found"));
    });

    it("200 - Payment information deleted: accActive:false", async () => {
      const body = {
        idPaymentInfo: "paym-001",
      };

      const res = await chai.request(app)
        .delete(route + "/deletePaymentInfo")
        .set("Cookie", cookie)
        .send(body);


      expect(() => res.to.throw("Your payment record has been deleted"));
    });
  });
});
