import { expect } from "chai";
import chai from "chai";
import { app } from "../../src/app.js";
import { businessJwt } from "../index.test.js";

const route = "/business/groupEmployee"
const cookie = "access_token=" + businessJwt

describe(route, () => {

    context("GET", () => {
      it("Should: 200 - Get all employee groups", async () => {
        const res = await chai.request(app)
          .get(route + "/getAll")
          .set("Cookie", cookie)

        expect(res.status).is.equal(200);
      });
    });

    context("POST", () => {
      it("400 - Invalid request body : missing group name.", async () => {
        const body = {
          groupName : ""
        };

        const res = await chai.request(app)
          .post(route + "/createGroup")
          .set("Cookie", cookie)
          .send(body)

        expect(res).to.have.status(400);
      });

      it("Group already exists in database", async () => {
        const body = {
          groupName: "Marketing",
        };

        const res = await chai.request(app)
        .post(route + "/createGroup")
        .set("Cookie", cookie)
        .send(body)

        expect(res).to.have.status(400);
      });

      it("Should: 200 - Create new employee group.", async () => {
        const body = {
          groupName: "IT",
        };

        const res = await chai.request(app)
        .post(route + "/createGroup")
        .set("Cookie", cookie)
        .send(body)

        expect(res).to.have.status(200);
      });
    });

    context("PUT", () => {
      it("400 - Invalid request body : missing group name.", async () => {
        const body = {
          idGroup: "empG-001",
          groupName: "",
          accActive: true
        };

        const res = await chai.request(app)
          .put(route + "/updateGroup")
          .set("Cookie", cookie)
          .send(body);

        expect(res.status).is.equal(400);
      });

      it("400 - Group not found", async () => {
        const body = {
          idGroup: "aas-000",
          groupName: "Directors",
          accActive: true,
        };
        
        const res = await chai.request(app)
        .put(route + "/updateGroup")
        .set("Cookie", cookie)
        .send(body);

        expect(res).to.have.status(400);
      });

      it("Should: 200 - Updated employee group.", async () => {
        const body = {
          idGroup: "empG-001",
          groupName: "Directors",
          accActive: true,
        };
        
        const res = await chai.request(app)
        .put(route + "/updateGroup")
        .set("Cookie", cookie)
        .send(body);

        expect(res).to.have.status(200);
      });
    });
});
