import chaiHttp from "chai-http";
import chai from "chai";
import jwt from "jsonwebtoken"
import {app} from "../src/app.js"
import { Account } from "../src/models/Account.js";
import { Employee } from "../src/models/Employee.js";
import { Business } from "../src/models/Business.js";
import { EmployeeGroup } from "../src/models/EmployeeGroup.js";
import { PaymentInfo } from "../src/models/PaymentInfo.js";
import { Transaction } from "../src/models/Transaction.js";
import { Invoice } from "../src/models/Invoice.js";

import { accounts } from "./data/accounts.js";
import { business } from "./data/business.js";
import { employeeGroup } from "./data/employeeGroup.js";
import { employees } from "./data/employees.js";
import { paymentInfo } from "./data/paymentInfo.js";
import { AccountType } from "../src/enums/AccountType.js";

chai.use(chaiHttp);
// export const req = chai.request("http://localhost:4000");
export const req = chai.request(app);

export const clientJwt = jwt.sign(
  {
    role : AccountType.CLIENT,
    email : "caven@gmail.com",
    sub : "168f9a04-a67b-49e9-8f87-fb0c1d299a80"
  }, 
  "secret"
)

export const businessJwt = jwt.sign(
  {
    role : AccountType.BUSINESS,
    email : "business1@gmail.com",
    sub : "f02b19e0-a1e6-48ed-97c6-60fabebda006"
  }, 
  "secret"
)


async function insertAll() {
  await Account.bulkCreate(accounts);
  await Business.bulkCreate(business);
  await EmployeeGroup.bulkCreate(employeeGroup);
  await Employee.bulkCreate(employees);
  await PaymentInfo.bulkCreate(paymentInfo);
}

async function deleteAll() {
  await PaymentInfo.destroy({ where: {} });
  await Transaction.destroy({ where : {}})
  await Invoice.destroy({ where : {}})
  await Employee.destroy({ where: {} });
  await EmployeeGroup.destroy({ where: {} });
  await Account.destroy({ where: {} });
  await Business.destroy({ where: {} });
}

beforeEach(async () => {
  await deleteAll();
  await insertAll();
});
