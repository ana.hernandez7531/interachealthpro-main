import { PayPalButtons, PayPalScriptProvider } from "@paypal/react-paypal-js"
import { Button, Form } from "react-bootstrap"
import { useApi } from "../../hooks/useApi"
import { useForm } from "../../hooks/useForm"

export function PayInvoice(){

    const clientId = "AToMyidKteo4ufgNCOLVDBCaG3cJVMD2lD0uFsJE9KXk3W9Y6EV8rFGr0fFF-mMrkrdpx3jjtodmaBqK"

    const {request} = useApi()
    const {form, setForm} = useForm({
        idInvoice : ""
    })

    const createOrder = async ()=>{
        const response = await request("/transaction/createOrder", "post", form)
        return response.orderId
    }

    const onApprove = async (data, actions)=>{
        const orderId = data.orderID
        const response = await request("/transaction/capturePayment", "post", {orderId })
    }

    return (
        <>
            <h3>Pay invoice</h3>
            <Form>
                <Form.Control className="my-3" type="text" placeholder="idInvoice" value={form.idInvoice} onChange={setForm("idInvoice")} />
            </Form>
            <PayPalScriptProvider options={{"client-id" : clientId, currency : "CAD"}}>
                <PayPalButtons  createOrder={createOrder} onApprove={onApprove} forceReRender={[form]}/>
            </PayPalScriptProvider>
        </>
    )
}