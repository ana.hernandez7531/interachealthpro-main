import { Button, Form } from "react-bootstrap";
import { useApi } from "../../hooks/useApi";
import { useForm } from "../../hooks/useForm";

export function RegisterBusiness(){

    const {request} = useApi()
    const {form, setForm} = useForm({
        email : "",
        nameEnt : "",
        nameContact : "",
        emailContact : "",
        telNumber : "",
        password : ""
    }) 

    const onSubmit = async (e)=>{
        e.preventDefault()
        request("/auth/register/business", "post", form)
    }

    return(
        <Form onSubmit={onSubmit}>
            <h3>Create Business</h3>
            <Form.Control className="my-3" type="text" placeholder="email" value={form.email} onChange={setForm("email")} />
            <Form.Control className="my-3" type="text" placeholder="business name" value={form.nameEnt} onChange={setForm("nameEnt")} />
            <Form.Control className="my-3" type="text" placeholder="name contact" value={form.nameContact} onChange={setForm("nameContact")} />
            <Form.Control className="my-3" type="text" placeholder="email contact" value={form.emailContact} onChange={setForm("emailContact")} />
            <Form.Control className="my-3" type="text" placeholder="telephone Number" value={form.telNumber} onChange={setForm("telNumber")} />
            <Form.Control className="my-3" type="text" placeholder="password" value={form.password} onChange={setForm("password")} />
            <Button className="my-3" variant="primary" type="submit">Submit</Button>
        </Form>
    )
}