import Keycloak from "keycloak-js"
import { useEffect, useRef } from "react"
import { Button, Form } from "react-bootstrap"
import { useDispatch } from "react-redux"
import { useApi } from "../../hooks/useApi"
import { useForm } from "../../hooks/useForm"
import { authSlice } from "../../redux/slices/authSlice"

export function Login(){

    const dispatch = useDispatch()
    const {request} = useApi()
    const {form, setForm} = useForm({
        email : "",
        password : ""
    }) 

    const onSubmit = async (e)=>{
        e.preventDefault()
        const response = await request("/auth/login", "post", {
            username : form.email,
            password : form.password
        })
        dispatch(authSlice.updateStatus(response))
    }

    return (
        <Form onSubmit={onSubmit}>
            <h3>Login client</h3>
            <Form.Control className="my-3" type="text" placeholder="email" value={form.email} onChange={setForm("email")} />
            <Form.Control className="my-3" type="password" placeholder="password" value={form.password} onChange={setForm("password")} />
            <Button className="my-3" variant="primary" type="submit">Login</Button>
        </Form>
    )
   
}