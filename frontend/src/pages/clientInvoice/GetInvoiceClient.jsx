import { PayPalButtons, PayPalScriptProvider } from "@paypal/react-paypal-js"
import { useEffect, useState } from "react"
import { useApi } from "../../hooks/useApi"

export function GetInvoiceClient(){
    const [state, setState] = useState([])
    const {request} = useApi()

    const showData = () => state.map(e => (
        <tr key={e.idInvoice}>
            <td>{e.idInvoice}</td>
            <td>{e.description}</td>
            <td>{e.price}</td>
            <td>{"" + e.isPayed}</td>
            <td>{e.lastUpdate}</td>
            <td>{e.idAcc}</td>
        </tr>
    )) 

    const getData = async ()=>{
        const response = await request("/client/invoice")

        if(response)
            setState(response)
    }

    useEffect(() => {
        getData()
    }, [])

    return(
        <>
            <h2>Get client invoices</h2>
            <table className="table">
                <thead>
                    <tr>
                        <th>idInvoice</th>
                        <th>description</th>
                        <th>price</th>
                        <th>isPayed</th>
                        <th>lastUpdate</th>
                        <th>idAcc</th>
                    </tr>
                </thead>
                <tbody>{showData()}</tbody>
            </table>
        </>
    )
}