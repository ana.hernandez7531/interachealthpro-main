import { useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";
import { useApi } from "../../hooks/useApi";
import { useForm } from "../../hooks/useForm"

export function PostEmployee(){

    const [idGroup, setGroup] = useState([])
    const {request} = useApi()
    const {form, setForm} = useForm({
        idAcc : "",
        idGroup : ""
    })

    const onSubmit = async (e)=>{
        e.preventDefault()

        const data = {
            ...form, 
            idGroup : form.idGroup === "" ? null : form.idGroup
        }
        request("/business/employee", "post", data)
    }

    const getEmployeeGroup = async ()=>{
        const response = await request("/business/groupEmployee/getAll")

        if(response)
            setGroup(response)
    }

    const showIdGroup = idGroup.map(e => <option key={e.idGroup} value={e.idGroup}>{`${e.idGroup} - ${e.groupName}`}</option>)


    useEffect(()=>{
        getEmployeeGroup()
    }, [])


    return (
        <Form onSubmit={onSubmit}>
            <h3>Create client</h3>
            <Form.Control className="my-3" type="text" placeholder="id account" value={form.idAcc} onChange={setForm("idAcc")} />
            <Form.Select value={form.idGroup} onChange={setForm("idGroup")}>
                <option value="">null - No idGroup</option>
                {showIdGroup}
            </Form.Select>
            <Button className="my-3" variant="primary" type="submit">Submit</Button>
        </Form>
    )
}