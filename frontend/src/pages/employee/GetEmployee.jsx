import { useEffect, useState } from "react"
import { useApi } from "../../hooks/useApi"

export function GetEmployee(){

    const [state, setState] = useState([])
    const {request} = useApi()

    const showData = () => state.map(e => (
        <tr key={e.idAcc}>
            <td>{e.idAcc}</td>
            <td>{"" + e.idGroup}</td>
            <td>{e.name}</td>
            <td>{e.lastName}</td>
            <td>{e.email}</td>
            <td>{"" + e.accActive}</td>
            <td>{e.lastUpdateDate}</td>
        </tr>
    )) 

    const getData = async ()=>{
        const response = await request("/business/employee")

        if(response)
            setState(response)
    }

    useEffect(() => {
        getData()
    }, [])

    return(
        <>
            <h2>Get employee</h2>
            <table className="table">
                <thead>
                    <tr>
                        <th>idAcc</th>
                        <th>idGroup</th>
                        <th>name</th>
                        <th>lastName</th>
                        <th>email</th>
                        <th>accActive</th>
                        <th>lastUpdateDate</th>
                    </tr>
                </thead>
                <tbody>{showData()}</tbody>
            </table>
        </>
    )
}