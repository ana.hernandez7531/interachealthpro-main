import { Button, Form } from "react-bootstrap";
import { useApi } from "../../hooks/useApi";
import { useForm } from "../../hooks/useForm";

export function PutEmployeeGroup() {
  const { request } = useApi();
  const { form, setForm } = useForm({
    idGroup: "",
    groupName: "",
    accActive: "",
  });

  const onSubmit = async (f) => {
    f.preventDefault();
    request("/business/groupEmployee/updateGroup", "put", form);
  };

  return (
    <Form onSubmit={onSubmit}>
      <h3>Update employee group</h3>

      <Form.Control
        className="my-3"
        type="text"
        placeholder="idGroup"
        value={form.idGroup}
        onChange={setForm("idGroup")}
      />
      <Form.Control
        className="my-3"
        type="text"
        placeholder="groupName"
        value={form.groupName}
        onChange={setForm("groupName")}
      />

      <Form.Control
        className="my-3"
        type="text"
        placeholder="accActive"
        value={form.accActive}
        onChange={setForm("accActive")}
      />

      <Button className="my-3" variant="primary" type="submit">
        Submit
      </Button>
    </Form>
  );
}
