import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { infoSlice } from "../redux/slices/infoSlice";
import {authSlice} from "../redux/slices/authSlice"


export function useApi(){
    
    const dispatch = useDispatch()
    const baseUrl = "http://localhost:4000"
    // const baseUrl = "https://interacthealthprowebservices.onrender.com"

    const logRequest = (route, data) =>
        dispatch(infoSlice.updateRequest({url : route, body : data}))
    
    const logResponse = (statusCode, body) => 
        dispatch(infoSlice.updateResponse({statusCode, body}))

    const options = (method, body) => {
        return {
            method,
            headers : {
                "content-type" : "application/json"
            }, 
            body : body ? JSON.stringify(body) : null,
            credentials : "include"
        }
    }

    const request = async (route, method = "get", data = null)=>{
        try{
            const url = baseUrl + route
            logRequest(url, data)

            const response = await fetch(url, options(method, data))
            const json = await response.json()
            logResponse(response.status, json)

            if(response.status === 401)
                dispatch(authSlice.reset())

            if(response.status === 200)
                return json
            else 
                return null

        } catch(error){
            logResponse(null, error)
        }
    }

    // Remove all logging from info slice
    useEffect(() => () => dispatch(infoSlice.reset()), [])

    return {request}
}
