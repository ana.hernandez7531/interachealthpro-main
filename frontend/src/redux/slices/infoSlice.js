import { createSlice } from "@reduxjs/toolkit";

const slice = createSlice({
    name : "infoSlice",

    initialState : {
        request : {
            url : null,
            body : null
        }, 

        response : {
            statusCode : null,
            body : null
        }
    },

    reducers : {
        updateRequest(state, action){
            state.request.url = action.payload.url
            state.request.body = JSON.stringify(action.payload.body, null, 1)
        },

        updateResponse(state, {payload}){
            state.response.statusCode = payload.statusCode
            state.response.body = JSON.stringify(payload.body, null, 10)
        },

        reset(state){
            state.request.url = null
            state.request.body = null
            state.response.statusCode = null
            state.response.body = null
        }
    }
})

export const infoSlice = slice.actions
export const infoReducer = slice.reducer